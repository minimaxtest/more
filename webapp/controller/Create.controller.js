sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History"
], function(BaseController, MessageBox, Utilities, History, MessageToast) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.myRepairNotification.controller.Create", {
		handleRouteMatched: function(oEvent) {
			var oParams = {};
			if (oEvent.mParameters.data.context) {
				this.sContext = oEvent.mParameters.data.context;
				var oPath;
				if (this.sContext) {
					oPath = {
						path: "/" + this.sContext,
						parameters: oParams
					};
					this.getView().bindObject(oPath);
				}
			}

		},
		_onPageNavButtonPress: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			var oQueryParams = this.getQueryParameters(window.location);

			if (sPreviousHash !== undefined || oQueryParams.navBackToLaunchpad) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("default", true);
			}

		},
		getQueryParameters: function(oLocation) {
			var oQuery = {};
			var aParams = oLocation.search.substring(1).split("&");
			for (var i = 0; i < aParams.length; i++) {
				var aPair = aParams[i].split("=");
				oQuery[aPair[0]] = decodeURIComponent(aPair[1]);
			}
			return oQuery;
		},
		doNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;

			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, false);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function(bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, false);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName);
			}

			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}

		},
		_onAttachmentPress: function(oEvent) {
					
			var oBindingContext = oEvent.getSource().getBindingContext();

			return new Promise(function(fnResolve) {

				this.doNavigate("Attachments", oBindingContext, fnResolve, "");
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});

		},
		_onSendPress: function(oEvent) {

			var sDialogName = "Dialog1";
			this.mDialogs = this.mDialogs || {};
			var oDialog = this.mDialogs[sDialogName];
			var oSource = oEvent.getSource();
			var oBindingContext = oSource.getBindingContext();
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oView;
			
			
			
			
			//check for text inputs
			if (!this.getView().byId("short").getValue() || !this.getView().byId("long").getValue()) {
				var sTargetPos = "center center";
				sap.m.MessageToast.show("Bitte geben Sie einen Kurz- und Langtext ein.", {
								duration: 4000 || 3000,
								at: sTargetPos,
								my: sTargetPos
							});
				return;
			}
			
			if (!oDialog) {
				this.getOwnerComponent().runAsOwner(function() {
					oView = sap.ui.xmlview({
						viewName: "com.sap.build.standard.myRepairNotification.view." + sDialogName
					});
					this.getView().addDependent(oView);
					oView.getController().setRouter(this.oRouter);
					oDialog = oView.getContent()[0];
					this.mDialogs[sDialogName] = oDialog;
				}.bind(this));
			}
	
			return new Promise(function(fnResolve) {
				oDialog.attachEventOnce("afterOpen", null, fnResolve);
				oDialog.open();
				if (oView) {
					oDialog.attachAfterOpen(function() {
						oDialog.rerender();
					});
				} else {
					oView = oDialog.getParent();
				}

				var oModel = this.getView().getModel();
				if (oModel) {
					oView.setModel(oModel);
				}
				if (sPath) {
					var oParams = oView.getController().getBindingParameters();
					oView.bindObject({
						path: sPath,
						parameters: oParams
					});
				}
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});

		},
		UploadChange: function(oEvent) {
			
			var oFile = oEvent.getSource();	
			var vFile = oFile.oFileUpload.files[0];
			
			this.addAttachment(vFile, oFile.getName());
			
			oFile.clear();
		},
		addAttachment: function(file, typ) {
			var reader = new FileReader();
			var that = this;
			var icon;
			if (typ === "Foto") {
				icon = "sap-icon://camera";
			}
			switch(typ) {
    			case "Foto":
    				icon = "sap-icon://camera";
    				break;
    			case "Skizze":
    				icon = "sap-icon://pushpin-on";
    				break;
    			case "PDF":
    				icon = "sap-icon://pdf-attachment";
    				break;
			}
			
			var extension = file.name.split(".");
			
			
			reader.onload = function(e) {
				var oModel = sap.ui.getCore().getModel("Noti");
				var aAttachment = oModel.getProperty("/Attachments");
				var vAttachment = { "ID"    : "",
									"Titel" : "",
									"ContentTyp"	: typ,
									"Icon"  : icon,
									"Path"	: e.target.result,
									"Extension" : extension[extension.length - 1]
				};
				aAttachment.push(vAttachment);
				oModel.setProperty("/Attachments", aAttachment);
				var vID = (aAttachment.length ? aAttachment.length - 1 : 0);
				
				//Go to detail page
				that.oRouter.navTo("Editattachment", { context:  vID }, false);
					
				
			};
			//reader.readAsBinaryString(file);
			reader.readAsDataURL(file);
		},
		onDataReceived: function(channel, event, data) {
			//save funcloc for further processing
			this.vFuncLoc = data;
			
			//set funcloc
			var oModel = sap.ui.getCore().getModel("Noti");
			oModel.setProperty("/FuncLoc", this.vFuncLoc);
			oModel.setProperty("/ShortText", "");
			oModel.setProperty("/Longtext", "");
			oModel.setProperty("/Attachments", []);
			
			
		},
		onInit: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("Create").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
			
			//get search parameters
			var eventBus = sap.ui.getCore().getEventBus();
			// 1. ChannelName, 2. EventName, 3. Function to be executed, 4. Listener
			eventBus.subscribe("FuncLocEvent", "onClickEvent", this.onDataReceived, this);
			
			//create Model and set
			var aModel = {
				"NotiNumber" : "",
				"ShortText" : "",
				"Longtext"  : "",
				"FuncLoc" : "",
				"Attachments": []
			};
			var oModel = new sap.ui.model.json.JSONModel(aModel);
			oModel.setDefaultBindingMode(sap.ui.model.BindingMode.TwoWay);
			sap.ui.getCore().setModel(oModel,"Noti");
			this.getView().setModel(sap.ui.getCore().getModel("Noti"), "Noti");
			this.getView().bindElement({ path: "/", model : "Noti"});
		}
	});
}, /* bExport= */ true);