sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History"
], function(BaseController, MessageBox, Utilities, History) {
	"use strict";
	return BaseController.extend("com.sap.build.standard.myRepairNotification.controller.Dialog1", {
		setRouter: function(oRouter) {
			this.oRouter = oRouter;

		},
		getBindingParameters: function() {
			return {};

		},
		createNotification: function() {
			var oModel = sap.ui.getCore().getModel("Noti");
			
			var oCreateModel = this.getView().getModel();
			
			var aRequest = {};
			//Header Data
			aRequest.NotiNumber = "";
			aRequest.ShortText = oModel.getProperty("/ShortText");
			aRequest.Longtext = oModel.getProperty("/Longtext");
			aRequest.FuncLoc = oModel.getProperty("/FuncLoc");
			
			//Attachment data
			var aAttachment = oModel.getProperty("/Attachments");
			var aToAttachment = [];
			
			aAttachment.forEach(function(att) {
				var toAtt = {};
				toAtt.ID = "";
				toAtt.Titel = att.Titel;
				//split Base64 content from path
				var content = att.Path.split("base64,");
				toAtt.ContentBase64 = content[1];
				toAtt.ContentType = att.Extension;
				toAtt.NotiNumber = "";
				aToAttachment.push(toAtt);
			});
			
			aRequest.ToAttachment = aToAttachment;
			
			oCreateModel.create("/RepairNotiSet", aRequest, null, function(requestBody,response){
             alert("Create successful:" + response);
         },function(){
             alert("Create failed");});                         
			
		},
		_onOKPress: function(oEvent) {

			oEvent = jQuery.extend(true, {}, oEvent);
			var that = this;
			return new Promise(function(fnResolve) {
					fnResolve(true);
				})
				.then(function(result) {
					var oDialog = this.getView().getContent()[0];

					return new Promise(function(fnResolve) {
						oDialog.attachEventOnce("afterClose", null, fnResolve);
						oDialog.close();
					});

				}.bind(this))
				.then(function(result) {
					this.oRouter.navTo("Search");
				}.bind(this))
				.then(function(result) {
					if (result === false) {
						return false;
					} else {
						return new Promise(function(fnResolve) {
							
							//bad style ... maybe register an event?
							that.createNotification();
							
							var sTargetPos = "center center";
							sTargetPos = (sTargetPos === "default") ? undefined : sTargetPos;
							sap.m.MessageToast.show("Ihre Reparaturmeldung wurde erfolgreich versendet", {
								onClose: fnResolve,
								duration: 4000 || 3000,
								at: sTargetPos,
								my: sTargetPos
							});
						});

					}
				}.bind(this)).catch(function(err) {
					if (err !== undefined) {
						MessageBox.error(err.message);
					}
				});
		},
		_onCancelPress: function() {
			var oDialog = this.getView().getContent()[0];

			return new Promise(function(fnResolve) {
				oDialog.attachEventOnce("afterClose", null, fnResolve);
				oDialog.close();
			});

		},
		onInit: function() {
			this._oDialog = this.getView().getContent()[0];
			this.getView().setModel(sap.ui.getCore().getModel("Noti"), "Noti");
			this.getView().bindElement({ path: "/", model : "Noti"});

		},
		onExit: function() {
			this._oDialog.destroy();
			
		}
	});
}, /* bExport= */ true);