sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History"
], function(BaseController, MessageBox, Utilities, History, MessageToast) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.myRepairNotification.controller.Editattachment", {
		deleteAttachment: function() {
			var oModel = sap.ui.getCore().getModel("Noti"); 
			var oData = oModel.getData();
			oData.Attachments.splice(this.sContext,1);
			oModel.setData(oData);
		},
		handleRouteMatched: function(oEvent) {
			if (oEvent.mParameters.data.context) {
				this.bNoDel = oEvent.mParameters.data.noDel;
				this.sContext = oEvent.mParameters.data.context;
				this.getView().bindElement({ path: "/Attachments/" + this.sContext + "/" , model : "Noti"}); 
				
			}
			
		},
		_onPageNavButtonPress: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			var oQueryParams = this.getQueryParameters(window.location);
	
			//delete image from model
			if (!this.bNoDel) {
				this.deleteAttachment();	
			}
			
			if (sPreviousHash !== undefined || oQueryParams.navBackToLaunchpad) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("default", true);
			}

		},
		getQueryParameters: function(oLocation) {
			var oQuery = {};
			var aParams = oLocation.search.substring(1).split("&");
			for (var i = 0; i < aParams.length; i++) {
				var aPair = aParams[i].split("=");
				oQuery[aPair[0]] = decodeURIComponent(aPair[1]);
			}
			return oQuery;

		},
		_onButtonPress: function(oEvent) {

			var oModel = this.getView().getModel("Noti");
			if (!oModel.getProperty("/Attachments/" + this.sContext + "/Titel")) {
				sap.m.MessageToast.show("Bitte einen Titel angeben!");
			}
			else {
				
				return new Promise(function(fnResolve) {
	
					this.doNavigate("Create", null, fnResolve, "");
					}.bind(this)).catch(function(err) {
						if (err !== undefined) {
							MessageBox.error(err.message);
						}
					});
				
			}
		},
		_onButtonPressDelete: function(oEvent) {
			
			//delete current Attachment 
			this.deleteAttachment();

			//go back
			window.history.go(-1);
			
		},
		doNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;

			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, false);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function(bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, false);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName);
			}

			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}

		},
		onInit: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("Editattachment").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));
			this.getView().setModel(sap.ui.getCore().getModel("Noti"), "Noti");
		}
	});
}, /* bExport= */ true);