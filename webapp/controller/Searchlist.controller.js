sap.ui.define(["sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"./utilities",
	"sap/ui/core/routing/History"
], function(BaseController, MessageBox, Utilities, History) {
	"use strict";

	return BaseController.extend("com.sap.build.standard.myRepairNotification.controller.Searchlist", {
		handleRouteMatched: function(oEvent) {
			var oParams = {};

			if (oEvent.mParameters.data.context) {
				this.sContext = oEvent.mParameters.data.context;
				var oPath;
				if (this.sContext) {
					oPath = {
						path: "/" + this.sContext,
						parameters: oParams
					};
					this.getView().bindObject(oPath);
				}
			}

		},
		_onPageNavButtonPress: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();
			var oQueryParams = this.getQueryParameters(window.location);

			if (sPreviousHash !== undefined || oQueryParams.navBackToLaunchpad) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("default", true);
			}

		},
		getQueryParameters: function(oLocation) {
			var oQuery = {};
			var aParams = oLocation.search.substring(1).split("&");
			for (var i = 0; i < aParams.length; i++) {
				var aPair = aParams[i].split("=");
				oQuery[aPair[0]] = decodeURIComponent(aPair[1]);
			}
			return oQuery;

		},
		_onObjectListItemPress: function(oEvent) {

			var vFuncLoc = oEvent.getSource().getSelectedItem().getIntro();

			return new Promise(function(fnResolve) {
				this.doNavigate("Create", null, fnResolve, "");
				
				var eventBus = sap.ui.getCore().getEventBus();
			    eventBus.publish("FuncLocEvent", "onClickEvent", vFuncLoc);
			}.bind(this)).catch(function(err) {
				if (err !== undefined) {
					MessageBox.error(err.message);
				}
			});

		},
		doNavigate: function(sRouteName, oBindingContext, fnPromiseResolve, sViaRelation) {
			var sPath = (oBindingContext) ? oBindingContext.getPath() : null;
			var oModel = (oBindingContext) ? oBindingContext.getModel() : null;

			var sEntityNameSet;
			if (sPath !== null && sPath !== "") {
				if (sPath.substring(0, 1) === "/") {
					sPath = sPath.substring(1);
				}
				sEntityNameSet = sPath.split("(")[0];
			}
			var sNavigationPropertyName;
			var sMasterContext = this.sMasterContext ? this.sMasterContext : sPath;

			if (sEntityNameSet !== null) {
				sNavigationPropertyName = sViaRelation || this.getOwnerComponent().getNavigationPropertyForNavigationWithContext(sEntityNameSet,
					sRouteName);
			}
			if (sNavigationPropertyName !== null && sNavigationPropertyName !== undefined) {
				if (sNavigationPropertyName === "") {
					this.oRouter.navTo(sRouteName, {
						context: sPath,
						masterContext: sMasterContext
					}, false);
				} else {
					oModel.createBindingContext(sNavigationPropertyName, oBindingContext, null, function(bindingContext) {
						if (bindingContext) {
							sPath = bindingContext.getPath();
							if (sPath.substring(0, 1) === "/") {
								sPath = sPath.substring(1);
							}
						} else {
							sPath = "undefined";
						}

						// If the navigation is a 1-n, sPath would be "undefined" as this is not supported in Build
						if (sPath === "undefined") {
							this.oRouter.navTo(sRouteName);
						} else {
							this.oRouter.navTo(sRouteName, {
								context: sPath,
								masterContext: sMasterContext
							}, false);
						}
					}.bind(this));
				}
			} else {
				this.oRouter.navTo(sRouteName);
			}

			if (typeof fnPromiseResolve === "function") {
				fnPromiseResolve();
			}

		},
		onSearchQuery: function(channel, event, data) {
			// recieve the filter data
			var json = data;
			var aFilters = [];
			var oFilter = null;
			//build the filter for oData read
			if (json.Ordernumber) {
				
				oFilter = new sap.ui.model.Filter("Ordernumber", sap.ui.model.FilterOperator.Contains, json.Ordernumber);
				aFilters.push(oFilter);
			}
			if (json.FuncLoc) {
				oFilter = new sap.ui.model.Filter("FuncLoc", sap.ui.model.FilterOperator.Contains, json.FuncLoc);
				aFilters.push(oFilter);
			}
			if (json.CustomerNumber) {
				oFilter = new sap.ui.model.Filter("CustomerNumber", sap.ui.model.FilterOperator.EQ, json.CustomerNumber);
				aFilters.push(oFilter);
			}
			if (json.CPFullname) {
				oFilter = new sap.ui.model.Filter("CPFullname", sap.ui.model.FilterOperator.Contains, json.CPFullname);
				aFilters.push(oFilter);
			}
			if (json.Postcode) {
				oFilter = new sap.ui.model.Filter("Postcode", sap.ui.model.FilterOperator.Contains, json.Postcode);
				aFilters.push(oFilter);
			}
			if (json.Street) {
				oFilter = new sap.ui.model.Filter("Street", sap.ui.model.FilterOperator.Contains, json.Street);
				aFilters.push(oFilter);
			}
			if (json.City) {
				oFilter = new sap.ui.model.Filter("City", sap.ui.model.FilterOperator.Contains, json.City);
				aFilters.push(oFilter);
			}
			var that = this;
			
			
			//call oData-Service 
			var oModel = this.getView().getModel();
			oModel.read("/FunctionalLocationSet", {
				filters: aFilters,
				success: function(oData) {
					var oDetailsModel = new sap.ui.model.json.JSONModel(oData.results);
					that.getView().setModel(oDetailsModel, "detailsModel");

				},
				error: function(oError) {
					var msg = jQuery.parseJSON( oError.responseText).error.innererror.errordetails[0].message;
					var sTargetPos = "center center";
					sap.m.MessageToast.show(msg, {
						duration: 4000 || 3000,
						at: sTargetPos,
						my: sTargetPos
					});
				}
			});
			//}
		},
		onInit: function() {
			this.oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this.oRouter.getTarget("Searchlist").attachDisplay(jQuery.proxy(this.handleRouteMatched, this));

			//get search parameters
			var eventBus = sap.ui.getCore().getEventBus();
			// 1. ChannelName, 2. EventName, 3. Function to be executed, 4. Listener
			eventBus.subscribe("SearchEvent", "onNavigateEvent", this.onSearchQuery, this);

		}
	});
}, /* bExport= */ true);